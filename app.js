$(function(){

list_of_modules = ["org.bibliome.alvisnlp.modules.Action",
"org.bibliome.alvisnlp.modules.AnchorTuples",
"org.bibliome.alvisnlp.modules.AnimalReader",
"org.bibliome.alvisnlp.modules.AntecedentChoice",
"org.bibliome.alvisnlp.modules.Assert",
"org.bibliome.alvisnlp.modules.CartesianProductTuples",
"org.bibliome.alvisnlp.modules.DisambiguateAlternatives",
"org.bibliome.alvisnlp.modules.EnrichedDocumentWriter",
"org.bibliome.alvisnlp.modules.ExpressionExtract",
"org.bibliome.alvisnlp.modules.FSOVFileReader",
"org.bibliome.alvisnlp.modules.FillDB",
"org.bibliome.alvisnlp.modules.GeniaReader",
"org.bibliome.alvisnlp.modules.GeniaWriter",
"org.bibliome.alvisnlp.modules.I2B2Reader",
"org.bibliome.alvisnlp.modules.INIST.XMLWriter2ForINIST",
"org.bibliome.alvisnlp.modules.LLLReader",
"org.bibliome.alvisnlp.modules.LayerComparator",
"org.bibliome.alvisnlp.modules.MergeLayers",
"org.bibliome.alvisnlp.modules.NGrams",
"org.bibliome.alvisnlp.modules.OBOReader",
"org.bibliome.alvisnlp.modules.OgmiosTokenizer",
"org.bibliome.alvisnlp.modules.OntoReif",
"org.bibliome.alvisnlp.modules.PorterStemmer",
"org.bibliome.alvisnlp.modules.PubTatorReader",
"org.bibliome.alvisnlp.modules.RegExp",
"org.bibliome.alvisnlp.modules.RelpWriter",
"org.bibliome.alvisnlp.modules.RemoveEquivalent",
"org.bibliome.alvisnlp.modules.RemoveOverlaps",
"org.bibliome.alvisnlp.modules.Sequence_Impl",
"org.bibliome.alvisnlp.modules.Species",
"org.bibliome.alvisnlp.modules.SplitOverlaps",
"org.bibliome.alvisnlp.modules.TabularExport",
"org.bibliome.alvisnlp.modules.TextFileReader",
"org.bibliome.alvisnlp.modules.TriPhase.ProminentConceptReporter",
"org.bibliome.alvisnlp.modules.WhatsWrongExport",
"org.bibliome.alvisnlp.modules.ab3p.Ab3P",
"org.bibliome.alvisnlp.modules.aggregate.AggregateValues",
"org.bibliome.alvisnlp.modules.alvisdb.AlvisDBIndexer",
"org.bibliome.alvisnlp.modules.alvisir2.AlvisIRIndexer",
"org.bibliome.alvisnlp.modules.alvisre.AlvisREPrepareCrossValidation",
"org.bibliome.alvisnlp.modules.ardb.ADBWriter",
"org.bibliome.alvisnlp.modules.biolg.BioLG",
"org.bibliome.alvisnlp.modules.bionlpst.BioNLPSTReader",
"org.bibliome.alvisnlp.modules.bionlpst.GeniaJSONReader",
"org.bibliome.alvisnlp.modules.cadixe.AlvisAEReader",
"org.bibliome.alvisnlp.modules.cadixe.AlvisAEReader2",
"org.bibliome.alvisnlp.modules.cadixe.ExportCadixeJSON",
"org.bibliome.alvisnlp.modules.ccg.CCGParser",
"org.bibliome.alvisnlp.modules.ccg.CCGPosTagger",
"org.bibliome.alvisnlp.modules.classifiers.SelectingElementClassifier",
"org.bibliome.alvisnlp.modules.classifiers.TaggingElementClassifier",
"org.bibliome.alvisnlp.modules.classifiers.TrainingElementClassifier",
"org.bibliome.alvisnlp.modules.clone.InsertContents",
"org.bibliome.alvisnlp.modules.clone.MergeSections",
"org.bibliome.alvisnlp.modules.clone.RemoveContents",
"org.bibliome.alvisnlp.modules.compare.CompareElements",
"org.bibliome.alvisnlp.modules.count.NewCount",
"org.bibliome.alvisnlp.modules.enju.EnjuParser",
"org.bibliome.alvisnlp.modules.enju.EnjuParser2",
"org.bibliome.alvisnlp.modules.geniatagger.GeniaTagger",
"org.bibliome.alvisnlp.modules.html.QuickHTML",
"org.bibliome.alvisnlp.modules.keyword.KeywordsSelector",
"org.bibliome.alvisnlp.modules.mapper.ElementMapper",
"org.bibliome.alvisnlp.modules.mapper.FileMapper",
"org.bibliome.alvisnlp.modules.mapper.FileMapper2",
"org.bibliome.alvisnlp.modules.mapper.OBOMapper",
"org.bibliome.alvisnlp.modules.pattern.PatternMatcher",
"org.bibliome.alvisnlp.modules.projectors.AttestedTermsProjector",
"org.bibliome.alvisnlp.modules.projectors.ElementProjector",
"org.bibliome.alvisnlp.modules.projectors.SimpleProjector",
"org.bibliome.alvisnlp.modules.projectors.TyDIProjector",
"org.bibliome.alvisnlp.modules.projectors.YateaProjector",
"org.bibliome.alvisnlp.modules.prolog.RunProlog",
"org.bibliome.alvisnlp.modules.pubannotation.PubAnnotationExport",
"org.bibliome.alvisnlp.modules.rdf.RDFExport",
"org.bibliome.alvisnlp.modules.rdf.RDFProjector",
"org.bibliome.alvisnlp.modules.script.Script",
"org.bibliome.alvisnlp.modules.segmig.SeSMig",
"org.bibliome.alvisnlp.modules.segmig.WoSMig",
"org.bibliome.alvisnlp.modules.shell.Shell",
"org.bibliome.alvisnlp.modules.shell.Shell2",
"org.bibliome.alvisnlp.modules.shell.browser.BrowserShell",
"org.bibliome.alvisnlp.modules.sql.SQLImport",
"org.bibliome.alvisnlp.modules.stanford.StanfordNER",
"org.bibliome.alvisnlp.modules.tabular.TabularReader",
"org.bibliome.alvisnlp.modules.tees.TEESClassify",
"org.bibliome.alvisnlp.modules.tees.TEESTrain",
"org.bibliome.alvisnlp.modules.tika.TikaReader",
"org.bibliome.alvisnlp.modules.tomap.TomapProjector",
"org.bibliome.alvisnlp.modules.tomap.TomapTrain",
"org.bibliome.alvisnlp.modules.treetagger.TreeTagger",
"org.bibliome.alvisnlp.modules.treetagger.TreeTaggerReader",
"org.bibliome.alvisnlp.modules.trie.ElementProjector2",
"org.bibliome.alvisnlp.modules.trie.OBOProjector",
"org.bibliome.alvisnlp.modules.trie.SimpleProjector2",
"org.bibliome.alvisnlp.modules.wapiti.WapitiLabel",
"org.bibliome.alvisnlp.modules.wapiti.WapitiTrain",
"org.bibliome.alvisnlp.modules.wok.WebOfKnowledgeReader",
"org.bibliome.alvisnlp.modules.xml.XMLReader",
"org.bibliome.alvisnlp.modules.xml.XMLReader2",
"org.bibliome.alvisnlp.modules.xml.XMLWriter",
"org.bibliome.alvisnlp.modules.xml.XMLWriter2",
"org.bibliome.alvisnlp.modules.yatea.YateaExtractor"]

list_metadata_fields = []

create_xml_parser = function(data){
  parser = new DOMParser();
  xmlDoc = parser.parseFromString(data,"text/xml");
  elements = xmlDoc.getElementsByTagNameNS('http://www.w3.org/2001/XMLSchema', 'element');
  for (i=0;i<elements.length;i++){
    name = elements[i].getAttribute('name');
    if (name != "null"){
      list_metadata_fields[i] = name;
    } else {
      list_metadata_fields[i] = elements[i].getAttribute('ref');
    }
  }

  add_metadata_fields();
}


window.setTimeout(function(){
$.ajax({url: 'https://raw.githubusercontent.com/openminted/omtd-share_metadata_schema/master/OMTD-SHARE%20v201%20XSD/OMTD-SHARE-Component.xsd', success: create_xml_parser});
}, 100);
//xmlDoc.getElementsByTagNameNS('http://www.w3.org/2001/XMLSchema', 'element');


metadata = {}

for (module of list_of_modules){
  metadata[module] = {};
  //for (field of list_metadata_fields){
  //  metadata[module][field] = '';
  //}
}

prefix = 'org.bibliome.alvisnlp.modules.';

// One module
$('body').append('<div">Sélectionnez un module :</p>')
  .append('<select id="select_module" onchange="choose_module()"></select>');

for (module of list_of_modules){
  option_html = '<option value="' + module + '">' + module.replace(RegExp(prefix), '') + '</option>';
  $('#select_module').append(option_html);
}

// and its metadata
add_metadata_fields = function() {

// authentication


$('body').append('<form id="authentication_form"></form>');
$('#authentication_form').append('<p>Nom d\'utilisateur<p><input id="username"></input><br />');
$('#authentication_form').append('<p>Mot de passe<p><input id="password" type="password"></input><br />');


// feedback

save_feedback = function(){
  if (localStorage.feedback_text) feedback_text = localStorage.feedback_text;
  else feedback_text = '';
  localStorage.setItem('feedback_text', feedback_text + '/////////////////' +  $('#feedback_text').val());

}

$('body').append('<form id="feedback_form"></form>');
$('#feedback_form').append('<div class="feedback" ><p>Entrez toute fonction que vous voulez voir</p><input id="feedback_text"></input><button onclick="save_feedback()">Envoyer la requête</button></div>');


// metadata form
$('body').append('<form id="metadata_form"><p>Pour ce module, entrez les métadonnées :</p></form>');

for (field of list_metadata_fields){

  input_html = '<div>' + field +'</div><input id="'+ field +'" class="metadata_input"  ></input><br />';
  $('#metadata_form').append(input_html);
}

$('body').append('<input id="keep_fields_checkbox" type="checkbox" checked="true">Conserver les champs</input> ');
$('body').append('<button id="save_form_btn" onclick="save_form()">Enregistrer</button');
//$('body').append('<button id="save_form_btn" onclick="save_form()">Enregistrer mais conserver les champs</form>');

}

choose_module = function(){
  save_form();
  module = $('#select_module option:selected').text();
}

save_form = function(keep){
  module = $('#select_module option:selected').text();
  for (field of list_metadata_fields){
    value = $('#' + field).val();
    if (value != ''){
      metadata[prefix + module][field] = value;
    }
  }
  username = $('#username').val();
  password = $('#password').val();
  query = 'SELECT distinct ?g WHERE {graph ?g {?s ?p ?o}}';
  // https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/overrideMimeType
  $.ajax({url: 'http://138.102.22.31:8890/sparql?query=' + encodeURI(query),
        crossOrigin: true,
        headers: {'Accept': 'appilaction/json'},
         success: function(data){console.log(data)},
        });
  console.log(username, password);



  // empty form if needed
  keep = $('#keep_fields_checkbox').prop('checked');
  if (keep === false){
    $('.metadata_input').val('');
  }
}























});
